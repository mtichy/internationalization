# Internationalization library

This library provides internationalized data types and utils.

## Installation

via composer: 
```sh
composer require mtichy/internationalization
```
or you can just download project source files directly

## Usage

with composer: 
```php
<?php
require __DIR__.'/vendor/autoload.php';
```

without composer 
```php
<?php
require 'path-to-I18N-dir/autoload.php';
```
