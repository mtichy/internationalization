<?php

namespace MTi\I18n\Type;


class CzechPeriod
{
    /** constants **/
    const SUFFIXES_MAPPING_YEARS  = 1;
    const SUFFIXES_MAPPING_MONTHS = 2;

    private static $suffixesMapping = array (
        0 => array (
            self::SUFFIXES_MAPPING_YEARS  => 'let',
            self::SUFFIXES_MAPPING_MONTHS => 'měsíců',
        ),
        1 => array (
            self::SUFFIXES_MAPPING_YEARS  => 'rok',
            self::SUFFIXES_MAPPING_MONTHS => 'měsíc',
        ),
        2 => array (
            self::SUFFIXES_MAPPING_YEARS  => 'roky',
            self::SUFFIXES_MAPPING_MONTHS => 'měsíce',
        ),
        3 => array (
            self::SUFFIXES_MAPPING_YEARS  => 'roky',
            self::SUFFIXES_MAPPING_MONTHS => 'měsíce',
        ),
        4 => array (
            self::SUFFIXES_MAPPING_YEARS  => 'roky',
            self::SUFFIXES_MAPPING_MONTHS => 'měsíce',
        ),
    );

    public function __construct(int $years = 0, int $months = 0)
    {
        $months = max($months, 0);
        $years_ = floor($months / 12);
        $this->years  = max($years, 0) + $years_;
        $this->months = $months - ($years_ * 12);
    }
    private $years;
    private $months;

    private function suffix(string $type, string $value): string
    {
        if ($value > 4 || $value == 0) {
            $value = 0;
        }
        return self::$suffixesMapping[$value][$type];
    }

    /**
     * @return string
     */
    public function yearsWithSuffix(): string
    {
        return sprintf(
            '%d %s'
          , $this->years
          , $this->suffix(
                self::SUFFIXES_MAPPING_YEARS
              , $this->years
            )
        );
    }

    /**
     * @return string
     */
    public function monthsWithSuffix(): string
    {
        return sprintf(
            '%d %s'
          , $this->months
          , $this->suffix(
                self::SUFFIXES_MAPPING_MONTHS
              , $this->months
            )
        );
    }

    /**
     * @return string
     */
    public function getMonthsAsYearsAndMonthsWithSuffix(): string
    {
        if ($this->months && !$this->years) {
            return $this->monthsWithSuffix();
        }
        return $this->months > 0
          ? sprintf(
                '%d %s a %d %s'
              , $this->years
              , $this->suffix(
                    self::SUFFIXES_MAPPING_YEARS
                  , $this->years
                )
              , $this->months
              , $this->suffix(
                    self::SUFFIXES_MAPPING_MONTHS
                  , $this->months
                )
            )
          : $this->yearsWithSuffix()
        ;
    }
}
