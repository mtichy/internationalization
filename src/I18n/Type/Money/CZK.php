<?php

namespace MTi\I18n\Type\Money;

use MTi\InvalidNumberException;
use MTi\Type\Money;


class CZK
    extends Money
{
    public static function createFromFloat(float $f, int $decimals = 2)
    {
        try {
            return new self($f, $decimals);
        }
        catch (InvalidNumberException $e) {
            throw new \LogicException();
        }
    }

    protected function withCurrency(string $v): string
    {
        return sprintf('%s %s', $v, 'Kč');
    }
}
