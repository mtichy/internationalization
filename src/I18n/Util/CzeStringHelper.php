<?php

namespace MTi\I18n\Util;

use MTi\RegexpException;
use MTi\Type\UnicodeString;


class CzeStringHelper
{
    private static function alphabet(): array
    {
        if (!self::$alphabet) {
            self::$alphabet = array_flip([
                '0','1','2','3','4','5','6','7','8','9',
                'a','á','b','c','č','d','ď','e','é','ě',
                'f','g','h','ch','i','í','j','k','l','m',
                'n','o','ó','ö','p','q','r','ř','s','š','t',
                'ť','u','ú','ů','v','w','x','y','ý','z','ž'
            ]);
        }
        return self::$alphabet;
    }
    private static $alphabet;

    private static function unCh(array $a)
    {
        foreach ($a as $k => $v) {
            if ($v == 'X') {
                $a[$k] = 'ch';
            }
        }
        return $a;
    }

    /**
     * @param UnicodeString|string $left
     * @param UnicodeString|string $right
     * @return int
     */
    public static function strCaseCmp($left, $right): int
    {
        if (!($left instanceof UnicodeString)) {
            $left = new UnicodeString($left);
        }
        if (!($right instanceof UnicodeString)) {
            $right = new UnicodeString($right);
        }
        try {
            $left = self::unCh(
                preg_split(
                    '/(?<!^)(?!$)/u'
                  , strval($left->lower()->replace('~ch~', 'X'))
                )
            );
            $right = self::unCh(
                preg_split(
                    '/(?<!^)(?!$)/u'
                  , strval($right->lower()->replace('~ch~', 'X'))
                )
            );
        }
        catch (RegexpException $e) {
            throw new \LogicException();
        }
        $max = min(count($left), count($right));
        $alphabet = self::alphabet();
        for ($i = 0; $i < $max; ++$i) {
            $lc = $left[$i];
            $rc = $right[$i];
            if (min(ord($lc), ord($rc)) < 48) {
                if (ord($lc) == ord($rc)) {
                    continue;
                }
                return ord($lc) > ord($rc) ? 1 : -1;
            }
            if ($alphabet[$lc] == $alphabet[$rc]) {
                continue;
            }
            return $alphabet[$left[$i]] > $alphabet[$right[$i]] ? 1 : -1;
        }
        return count($left) > count($right) ? 1 : -1;
    }

    /**
     * @param string $s
     * @return string Encoding name
     */
    public static function detectEncoding(string $s): string
    {
        if (preg_match('#[\x80-\x{1FF}\x{2000}-\x{3FFF}]#u', $s)) {
            return 'UTF-8';
        }
        if (preg_match('#[\x7F-\x9F\xBC]#', $s)) {
            return 'WINDOWS-1250';
        }
        return 'ISO-8859-2';
    }
}
