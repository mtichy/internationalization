<?php

namespace MTi\UnitTest;

use MTi\I18n\Type\Money\CZK;
use PHPUnit\Framework\TestCase;


class MoneyTest
    extends TestCase
{
    public function testCzk()
    {
        $m = new CZK(120);
        self::assertEquals('120,00 Kč', $m->formattedNumberWithCurrency());
        self::assertEquals('120,- Kč', $m->formattedNumberWithCurrencyAndHyphen());
    }
}
