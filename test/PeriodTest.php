<?php

namespace MTi\UnitTest;

use MTi\I18n\Type\CzechPeriod;
use PHPUnit\Framework\TestCase;


class PeriodTest
    extends TestCase
{
    public function dataProvider()
    {
        return [
            [1, 1, '1 rok', '1 měsíc', '1 rok a 1 měsíc'],
            [1, 2, '1 rok', '2 měsíce', '1 rok a 2 měsíce'],
            [2, 3, '2 roky', '3 měsíce', '2 roky a 3 měsíce'],
            [5, 5, '5 let', '5 měsíců', '5 let a 5 měsíců'],
        ];
    }

    /**
     * @dataProvider dataProvider
     *
     * @param int $y
     * @param int $m
     * @param string $t1
     * @param string $t2
     * @param string $t3
     */
    public function testPeriod(int $y, int $m, string $t1, string $t2, string $t3)
    {
        $p = new CzechPeriod($y, $m);
        self::assertEquals($t1, $p->yearsWithSuffix());
        self::assertEquals($t2, $p->monthsWithSuffix());
        self::assertEquals($t3, $p->getMonthsAsYearsAndMonthsWithSuffix());
    }
}
